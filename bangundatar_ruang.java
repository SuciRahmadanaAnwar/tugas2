import java.util.*;
public class bangundatar_ruang {
	public static void menu(){
		Scanner input = new Scanner(System.in);
		do{
    		String choice="x";
        	int pilihan=-1;
        System.out.println("\n");
		System.out.println("---------Bangun Datar dan Bangun Ruang---------");
		System.out.println("***********************************************"); 		
		System.out.println("1. Bangun Datar");
		System.out.println("2. Bangun Ruang");
		System.out.println("0. Keluar");
		System.out.println("***********************************************");
			System.out.print("Silahkan masukkan nomor yang anda pilih : ");
			try{
    			pilihan = input.nextInt();	    			
    		}catch(InputMismatchException e){
    			System.out.println("\nMaaf inputan yang anda masukkan bukan bilangan bulat");
    			System.out.print("DAN ");
    		}	    		
	
		 input.nextLine();			
		    	
		switch(pilihan){
		case 0:
			break;
		case 1:
			bangun_datar();
			break;
		case 2:   			
            bangun_ruang();
            break;
		default:
			System.out.println("Tidak sesuai dengan pilihan yang ada :)");
			do{ 
			    System.out.print("\nIngin Coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("=================================");
			    
				    if (choice.contentEquals("YA")){
				    	break;
				    }
				    else if(choice.contentEquals("TIDAK")){
				    	break;
				    }
				    else {
				    	System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
				    }
				    
	    	}while( (choice.contentEquals("YA") == false ) || ( choice.contentEquals("TIDAK") == false) );
			break;
		}
				if (pilihan == 0)break;
			    if (choice.contentEquals("TIDAK"))break;
			    if (choice.contentEquals("YA"))continue;
			
		}while(true);
	}
	
	public static void bangun_datar(){
		Scanner input = new Scanner (System.in);
		do{
			String choice="x";
			int pilihan=-1;
			
		System.out.println("\n");
		System.out.println("-----------------Bangun Datar------------------");
		System.out.println("***********************************************"); 	
		System.out.println("1. Persegi");
		System.out.println("2. Persegi Panjang");
		System.out.println("3. Trapesium");
		System.out.println("4. Segitiga sama kaki");
		System.out.println("5. Belah Ketupat");
		System.out.println("6. Lingkaran");
		System.out.println("7. Layang-layang");
		System.out.println("8. Jajargenjang");
		System.out.println("9. Segitiga sama sisi");
		System.out.println("0. Keluar");
		System.out.println("***********************************************"); 	
			System.out.print("Silahkan masukkan nomor yang anda pilih : ");
			try{
				pilihan = input.nextInt();	    			
			}catch(InputMismatchException e){
				System.out.println("\nMaaf inputan yang anda masukkan bukan bilangan bulat");
				System.out.print("DAN ");
		}	    		
			input.nextLine();			
	    	
		switch(pilihan){
		case 0:
			break;
		
		case 1:
			square();
			break;
			
		case 2:
			rectangle();
			break;
			
		case 3:
			trapezoid();
			break;
			
		case 4:
			IsoscelesTriangle();
			break;
			
		case 5:
			rhombus();
			break;
			
		case 6:
			circle();
			break;
			
		case 7:
			kyte();
			break;
			
		case 8:
			parallelogram();
			break;
			
		case 9:
			EquilateralTriangle();
			break;
			
		default:
			System.out.println("Tidak sesuai dengan pilihan yang ada :)");
			
		do{ 
		    System.out.print("\nIngin Coba lagi ? (YA/TIDAK)");
		    System.out.println();			   
		    choice=input.nextLine();
		    System.out.println("=================================");
		    
			    if (choice.contentEquals("YA")){
			    	break;
			    }
			    else if(choice.contentEquals("TIDAK")){
			    	break;
			    }
			    else {
			    	System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }
			    
    	}while( (choice.contentEquals("YA") == false ) || ( choice.contentEquals("TIDAK") == false) );
			break;
		}
			if (pilihan == 0)break;
		    if (choice.contentEquals("TIDAK"))break;
		    if (choice.contentEquals("YA"))continue;
		
		}while(true);
	}
	
	public static void square(){
		Scanner input = new Scanner (System.in);
		
		float side;
		String choice;
		do{
			try{
				System.out.println();
				System.out.print("Masukkan panjang sisi : ");
				side = input.nextFloat();
				
				if (side < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	
		    	if (side == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	
		    	System.out.println();
				
				double large = side*side;
				System.out.println("Luas dari persegi adalah "+large);
				double perimeter = 4*side;
				System.out.println("Keliling dari persegi adalah "+perimeter);
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
			    
		}while (true);	
	}
	public static void rectangle(){
		Scanner input = new Scanner (System.in);
		float length, width;
		String choice;
		do{
			try{
				System.out.println("Masukkan panjang persegi panjang : ");
				length = input.nextFloat();
				
		    	if (length < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (length == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	
		    	System.out.println("Masukkan lebar persegi panjang : ");
		    	width  = input.nextFloat();
				if (width < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (width == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	
		    	System.out.println();
				
				double large = length*width;
				System.out.println("Luas dari persegi panjang adalah "+large);
				double perimeter = (2*length)+(2*width);
				System.out.println("Keliling dari persegi panjang adalah "+perimeter);
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;

		}while (true);	
	}
	public static void trapezoid(){
		Scanner input = new Scanner (System.in);
		
		float side1,side2, side3, side4, height;
		String choice;
		do{
			try{
				System.out.println("Masukkan sisi atas trapesium : ");
				side1 = input.nextFloat();
				
				if (side1 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side1 == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	
		    	System.out.println("Masukkan sisi bawah trapesium : ");
				side2 = input.nextFloat();
				
				if (side2 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side2 == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	System.out.println("Masukkan sisi miring trapesium : ");
				side3 = input.nextFloat();
				
				if (side3 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side3 == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	System.out.println("Masukkan sisi miring trapesium : ");
				side4 = input.nextFloat();
				
				if (side4 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side4 == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	System.out.println("Masukkan tinggi trapesium : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}	
				
				double large = 0.5*((side1+side2)*height);
				System.out.println("Luas dari trapesium adalah "+large);
				double perimeter = side1*side2*side3*side4;
				System.out.println("Keliling dari Trapesium adalah "+perimeter);
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;	
			
		}while (true);
	}
	public static void IsoscelesTriangle(){
		Scanner input = new Scanner (System.in);

		float base,side,height;
		String choice;
		do{
			try{
				System.out.println("Masukkan panjang alas segitiga : ");
				base = input.nextFloat();
				
				if (base < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (base == 0) {
		    	    throw new ArithmeticException();
		    	}
		    	System.out.println("Masukkan panjang sisi miring segitiga : ");
				side = input.nextFloat();
				
				if (side < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan tinggi segitiga : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}
			
				double large = 0.5*(base*height);
				System.out.println("Luas dari segitiga sama kaki adalah "+large);
				double perimeter = base+(2*side);
				System.out.println("Keliling dari segitiga sama kaki adalah "+perimeter);
				
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
		    
		}while (true);	
	}
	public static void rhombus(){
		Scanner input = new Scanner (System.in);
		
		float diagonal1, diagonal2, side;
		String choice;
		do{
			try{
				System.out.println("Masukkan panjang diagonal 1 belah ketupat : ");
				diagonal1 = input.nextFloat();
				
				if (diagonal1 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (diagonal1 == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan panjang diagonal 2 belah ketupat: ");
				diagonal2 = input.nextFloat();
				
				if (diagonal2 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (diagonal2 == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan panjang sisi belah ketupat : ");
				side = input.nextFloat();
				
				if (side < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				double large = 0.5*(diagonal1*diagonal2);
				System.out.println("Luas dari belah ketupat adalah "+large);
				double perimeter = 4*side;
				System.out.println("Keliling dari belah ketupat adalah "+perimeter);
				
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
		    
		}while (true);
	}
	public static void circle(){
		Scanner input = new Scanner (System.in);
		
		float radius;
		String choice;
		do{
			try{
				System.out.println("Masukkan jari-jari Lingkaran : ");
				radius = input.nextFloat();
				
				if (radius < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (radius == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				double large = 3.14*(radius*radius);
				System.out.println("Luas dari Lingkaran adalah "+large);
				double perimeter = 2*(3.14*radius);
				System.out.println("Keliling dari Lingkaran adalah "+perimeter);
				
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
			
		}while (true);
	}
	public static void kyte(){
		Scanner input = new Scanner (System.in);
		
		float diagonal1, diagonal2, side1, side2;
		String choice;
		do{
			try{
				System.out.println("Masukkan panjang diagonal 1 Layang-layang : ");
				diagonal1 = input.nextFloat();
				
				if (diagonal1 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (diagonal1 == 0) {
		    	    throw new ArithmeticException();
		    	}
		    	
				System.out.println("Masukkan panjang diagonal 2 Layang-layang: ");
				diagonal2 = input.nextFloat();
				
				if (diagonal2 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (diagonal2 == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan sisi panjang : ");
				side1 = input.nextFloat();
				
				if (side1 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side1 == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan sisi pendek : ");
				side2 = input.nextFloat();
				
				if (side2 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side2 == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				double large = 0.5*(diagonal1*diagonal2);
				System.out.println("Luas dari Layang-layang adalah "+large);
				double perimeter = (2*side1)+(2*side2);
				System.out.println("Keliling dari Layang-layang adalah "+perimeter);
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
			
		}while (true);	
	}
	public static void parallelogram(){
		Scanner input = new Scanner (System.in);
		
		float base, height, side;
		String choice;
		do{
			try{
				System.out.println("Masukkan alas jajargenjang : ");
				base = input.nextFloat();
				
				if (base < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (base == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan tinggi jajargenjang : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan sisi miring jajargenjang : ");
				side = input.nextFloat();
				
				if (side < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				double LuasJajargenjang = base*height;
				System.out.println("Luas dari jajargenjang adalah "+LuasJajargenjang);
				double KelilingJajargenjang = (2*base)+(2*side);
				System.out.println("Keliling dari jajargenjang adalah "+KelilingJajargenjang);
				
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
			
		}while (true);
	}
	public static void EquilateralTriangle(){
		Scanner input = new Scanner (System.in);
		
		float side, height;
		String choice;
		do{
			try{
				System.out.println("Masukkan panjang sisi segitiga : ");
				side = input.nextFloat();
				
				if (side < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan tinggi segitiga : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				double Large = 0.5*(side*height);
				System.out.println("Luas dari segitiga sama sisi adalah "+Large);
				double perimeter = 3*side;
				System.out.println("Keliling dari segitiga sama sisi adalah "+perimeter);
			
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
			
		}while (true);
	}
	public static void bangun_ruang(){
		Scanner input = new Scanner (System.in);
		do{
			int pilihan=-1;
			String choice = "x";
			System.out.println("\n");
			System.out.println("-----------------Bangun Ruang------------------");
			System.out.println("***********************************************"); 	
			System.out.println("1. Balok");
			System.out.println("2. Kubus");
			System.out.println("3. Tabung");
			System.out.println("4. Kerucut");
			System.out.println("5. Prisma Segitiga");
			System.out.println("6. Limas segiempat");
			System.out.println("7. Bola");
			System.out.println("0. Keluar");
			System.out.println("***********************************************"); 
				System.out.print("Silahkan masukkan nomor yang anda pilih : ");
				try{
					pilihan = input.nextInt();	    			
				}catch(InputMismatchException e){
					System.out.println("\nMaaf inputan yang anda masukkan bukan bilangan bulat");
					System.out.print("DAN ");
				}	    		
			input.nextLine();			
		switch (pilihan){
			case 0:
				break;
			case 1:
				beam();
				break;
				
			case 2:
				cube();
				break;
				
			case 3:
				tube();
				break;
				
			case 4:
				cone();
				break;
				
			case 5:
				triangularprism();
				break;
				
			case 6:
				rectangularpyramid();
				break;
				
			case 7:
				ball();
				break;
		
			default:
				System.out.println("Tidak sesuai dengan pilihan yang ada :)");
		
	do{ 
	    System.out.print("\nIngin Coba lagi ? (YA/TIDAK)");
	    System.out.println();			   
	    choice=input.nextLine();
	    System.out.println("=================================");
	    
		    if (choice.contentEquals("YA")){
		    	break;
		    }
		    else if(choice.contentEquals("TIDAK")){
		    	break;
		    }
		    else {
		    	System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }
		    
	}while( (choice.contentEquals("YA") == false ) || ( choice.contentEquals("TIDAK") == false) );
		break;
	}
		if (pilihan == 0)break;
	    if (choice.contentEquals("TIDAK"))break;
	    if (choice.contentEquals("YA"))continue;
	
		}while(true);
	}
	
	public static void beam(){
		Scanner input = new Scanner (System.in);
		
		float length, widht, height;
		String choice;
		
		do{
			try{
				System.out.println("Masukkan panjang balok : ");
				length = input.nextFloat();
				
				if (length < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	
		    	if (length == 0) {
		    	    throw new ArithmeticException();
		    	}	
				
				System.out.println("Masukkan lebar balok : ");
				widht = input.nextFloat();
				
				if (widht < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	
		    	if (widht == 0) {
		    	    throw new ArithmeticException();
		    	}	
				
				System.out.println("Masukkan tinggi balok : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	System.out.println();
		    	
		    	double Volume = length*widht*height;
				System.out.println("Volume dari Balok adalah "+Volume);
				double surfArea = 2*((length*widht)+(length*height)+(widht*height));
				System.out.println("Luas Permukaan Balok adalah "+surfArea);
			
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
			    
		
		}while (true);
	}
		
	public static void cube(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan sisi kubus : ");
		float side;
		String choice;
		do{
			try{
				side = input.nextFloat();

		    	if (side < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	
		    	if (side == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	System.out.println();
		    	
		    	double Volume = side*side*side;
				System.out.println("Volume dari Kubus adalah "+Volume);
				double SurfArea = 6*(side*side);
				System.out.println("Luas Permukaan Kubus adalah "+SurfArea);
		    	
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
			    
			
		}while (true);	
	}
	public static void tube(){
		Scanner input = new Scanner (System.in);
		
		float radius, height;
		String choice;
		do{
			try{
				System.out.println("Masukkan jari-jari Tabung : ");
				radius = input.nextFloat();
				
				if (radius < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (radius == 0) {
		    	    throw new ArithmeticException();
		    	}	
			
				System.out.println("Masukkan tinggi Tabung : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}	
				System.out.println();
				
				double Volume = 3.14*((radius*radius)*height);
				System.out.println("Volume dari Tabung adalah "+Volume);
				double surfArea = 2*3.14*(radius*(radius+height));
				System.out.println("Luas Permukaan Tabung adalah "+surfArea);
				
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
		    
		}while (true);
	}
	public static void cone(){
		Scanner input = new Scanner (System.in);
		
		float radius, side, height;
		String choice;
		do{
			try{
				System.out.println("Masukkan jari-jari kerucut : ");
				radius = input.nextFloat();
				
				if (radius < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (radius == 0) {
		    	    throw new ArithmeticException();
		    	}
		    	
		    	System.out.println("Masukkan garis pelukis : ");
				side = input.nextFloat();
				
				if (side < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side == 0) {
		    	    throw new ArithmeticException();
		    	}
		    	
		    	System.out.println("Masukkan tinggi kerucut : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}	
		    	System.out.println();
		    	
				double Volume = ((1/3)*3.14*((radius*radius)*height));
				System.out.println("Volume dari Kerucut adalah "+Volume);
				double surfArea = 3.14*radius*(radius+side);
				System.out.println("Luas Permukaan Kerucut adalah "+surfArea);
			
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
		    
		}while (true);
	}
	public static void triangularprism(){
		Scanner input = new Scanner (System.in);
		
		float side1, side2, side3, height;
		String choice;
		do{
			try{
				System.out.println("Masukkan panjang sisi alas pertama : ");
				side1 = input.nextFloat();
				
				if (side1 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side1 == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan panjang sisi alas kedua : ");
				side2 = input.nextFloat();
				
				if (side2 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side2 == 0) {
		    	    throw new ArithmeticException();
		    	}
				
				System.out.println("Masukkan panjang sisi alas ketiga : ");
				side3 = input.nextFloat();
				
				if (side3 < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side3 == 0) {
		    	    throw new ArithmeticException();
		    	}
				System.out.println("Masukkan tinggi prisma : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}
				System.out.println();
				
				float perimeter = 2*(side1+side2+side3)+3*height;
				double s = 0.5*perimeter;
				double largetriangle = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
				double volume = largetriangle*height;
				System.out.println("Volume dari Prisma Segitiga adalah "+volume);
				double surfArea = (side1+side2+side3)*height*2*largetriangle;
				System.out.println("Luas Permukaan Prisma Segitiga adalah "+surfArea);
			
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
		    
		}while (true);	
	}
	public static void rectangularpyramid(){
		Scanner input = new Scanner (System.in);
		
		float side, height;
		String choice;
		do{
			try{
				System.out.println("Masukkan panjang sisi alas : ");
				side = input.nextFloat();
				
				if (side < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (side == 0) {
		    	    throw new ArithmeticException();
		    	}
		    	
		    	System.out.println("Masukkan tinggi limas : ");
				height = input.nextFloat();
				
				if (height < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (height == 0) {
		    	    throw new ArithmeticException();
		    	}
				System.out.println();
				
				double largepedestal = Math.pow(side,2);
				double heightside = Math.sqrt(Math.pow(0.5*side,2)*Math.pow(height,2));
				double largeside = 0.5*side*heightside;
				double volume = 1/3*largepedestal*height;
				System.out.println("Volume dari Limas Segiempat adalah "+volume);
				double surfArea = largepedestal+(4*largeside);
				System.out.println("Luas Permukaan Prisma Segitiga adalah "+surfArea);
				
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
		    
		}while (true);
	}
	public static void ball(){
		Scanner input = new Scanner (System.in);
		
		float radius;
		String choice;
		do{
			try{
				System.out.println("Masukkan jari-jari Bola : ");
				radius = input.nextFloat();
				
				if (radius < 0) {
		    	    throw new IllegalArgumentException();
		    	}
		    	if (radius == 0) {
		    	    throw new ArithmeticException();
		    	}
				System.out.println();
				
				double Volume = ( (double) 4/3 ) * 3.14 * radius * radius * radius;
				System.out.println("Volume dari Bola adalah "+Volume);
				double SurfArea = 4 * 3.14 * radius * radius;
				System.out.println("Luas permukaan dari Bola adalah "+SurfArea);
			
			}
		    catch(InputMismatchException e){
		    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
		    }
		    catch(ArithmeticException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
		    }
		    catch(IllegalArgumentException e){
		    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
		    }
		    input.nextLine();
		    do{ 
		    	System.out.println();
			    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("==============================");
				    if ( choice.contentEquals("YA") )break;
				    else if( choice.contentEquals("TIDAK") )break;
				    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
		    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
		    
		    if ( choice.contentEquals("YA") )continue;
		    else if( choice.contentEquals("TIDAK") )break;
			
		}while (true);
	}
	public static void main(String[] args) {
		menu();

	}

}
