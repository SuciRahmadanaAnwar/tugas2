import java.util.*;
public class tictactoe {
	static Scanner input=new Scanner(System.in);
	static void playgame(){
		try{
			int number=-1,player=1,select;
			char[][]table=new char[3][3];
			for(int rows=0;rows<3;rows++){
				for(int colomn=0;colomn<3;colomn++){
					table[rows][colomn]=' ';
				}
			}
			do{
				String choice2="a";
				do{
					String choice="a";
					for(int rows=0;rows<3;rows++){
						for(int colomn=0;colomn<3;colomn++){
							table[rows][colomn]=' ';
						}
					}
					System.out.println("Pilih yang ingin anda gunakan ( x / o )");
					try{
						choice=input.nextLine();
						if(choice.contentEquals("x") == false && choice.contentEquals("o") == false){
							throw new InputMismatchException();
						}
					}catch(InputMismatchException e){
						System.out.println("==========================================");
						System.out.println("Silahkan input salah satunya 'x' atau 'o' ");
					}catch(NoSuchElementException e){
						break;
					}
					
						//this block determines whether to write 'x' or 'o'
						if(choice.contentEquals("x")){
							number=1;
							break;
						}
						if(choice.contentEquals("o")){
							number=0;
							break;
						}
						////
						
				}while(true);
				
				while(true){
					int threshold=0;
					System.out.println("------------------------------------");
					System.out.println("\tPlayer "+player+"'s turn");
					System.out.println("\n");
					System.out.println("\t  "+table[0][0]+" |   "+table[0][1]+"   | "+table[0][2]);
					System.out.println("\t_ _ |_ _ _ _| _ _");
					System.out.println("\t  "+table[1][0]+" |   "+table[1][1]+"   | "+table[1][2]);
					System.out.println("\t_ _ |_ _ _ _| _ _");
					System.out.println("\t  "+table[2][0]+" |   "+table[2][1]+"   | "+table[2][2]);
					System.out.println("\t    |       |    ");
					System.out.println("\n");
					
				
					if(table[0][0]!=' ' && table[0][0]==table[0][1] && table[0][0]==table[0][2]||
							table[1][0]!=' ' && table[1][0]==table[1][1] && table[1][0]==table[1][2]||
							table[2][0]!=' ' && table[2][0]==table[2][1] && table[2][0]==table[2][2]||
							table[0][0]!=' ' && table[0][0]==table[1][1] && table[0][0]==table[2][2]||
							table[0][2]!=' ' && table[0][2]==table[1][1] && table[0][2]==table[2][0]||
							table[0][0]!=' ' && table[0][0]==table[1][0] &&	table[0][0]==table[2][0]||
							table[0][1]!=' ' && table[0][1]==table[1][1] && table[0][1]==table[2][1]||
							table[0][2]!=' ' && table[0][2]==table[1][2] && table[0][2]==table[2][2])
					{
						if(player==1)player++;
						else if(player==2)player--;
						System.out.println("PERMAINAN SELESAI");
						System.out.println("==========================================");
						System.out.println("----------PEMAIN "+player+" MENANG--------");
						System.out.println("==========================================");
						do{
							try{
								System.out.println("Ingin bermain kembali ? (ya/tidak)");
								choice2=input.nextLine();
								
								if(choice2.contentEquals("ya") == false && choice2.contentEquals("tidak") == false){
									throw new InputMismatchException();
								}
							}
							catch(InputMismatchException e){
								System.out.println("===============================================");
								System.out.println("Silahkan input salah satunya 'ya' atau 'tidak' ");
								input.nextLine();
								continue;
							}break;
						}while(true);
						
						break;
					}
					
					
					for(int rows=0;rows<3;rows++){
						for(int colomn=0;colomn<3;colomn++){
							if(table[rows][colomn]!=' '){
								threshold++;
							}
						}
					}if (threshold==9){
						System.out.println("==================================================");
						System.out.println("----------PERMAINAN SERI, TIDAK ADA MENANG--------");
						System.out.println("==================================================");
						break;
					}
					
					
					System.out.println("Masukkan salah satu nomor di bawah ini untuk mengisi papan permainan  :");
					if(table[0][0]==' '){
						System.out.println("1. Baris 1 Kolom 1");
					}
					if(table[0][1]==' '){
						System.out.println("2. Baris 1 Kolom 2");
					}
					if(table[0][2]==' '){
						System.out.println("3. Baris 1 Kolom 3");
					}
					if(table[1][0]==' '){
						System.out.println("4. Baris 2 Kolom 1");
					}
					if(table[1][1]==' '){
						System.out.println("5. Baris 2 Kolom 2");
					}
					if(table[1][2]==' '){
						System.out.println("6. Baris 2 Kolom 3");
					}
					if(table[2][0]==' '){
						System.out.println("7. Baris 3 Kolom 1");
					}
					if(table[2][1]==' '){
						System.out.println("8. Baris 3 Kolom 2");
					}
					if(table[2][2]==' '){
						System.out.println("9. Baris 3 Kolom 3");
					}
					do{
						
						System.out.print("Silahkan masukkan Nomor yang anda pilih :  ");
						select=-1;
						try{
							select=input.nextInt();
							break;
						}catch(InputMismatchException e){
							System.out.println();
							System.out.println("Maaf inputan yang anda masukkan bukan angka");
							input.nextLine();
							select=-1;
							continue;
						}
					}while(true);
					
					switch(select){
					case 1:
						if(table[0][0]==' ' && select==1){
							if(number==1){
								table[0][0]='x';
								number--;
							}
							else if(number==0){
								table[0][0]='o';
								number++;
							}
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[0][0]!=' ' && select==1){
							System.out.println("Silahkan mengisi satu ruang yang kosong ");
						}break;
					case 2:
						if(table[0][1]==' ' && select==2){
							if(number==1){
								table[0][1]='x';
								number--;
							}
							else if(number==0){
								table[0][1]='o';
								number++;
							}
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[0][1]!=' ' && select==2){
							System.out.println("Silahkan mengisi satu ruang yang kosong ");
						}break;
					case 3:
						if(table[0][2]==' ' && select==3){
							if(number==1){
								table[0][2]='x';
								number--;
							}
							else if(number==0){
								table[0][2]='o';
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[0][2]!=' ' && select==3){
							System.out.println("Silahkan mengisi satu ruang yang kosong ");
						}
					case 4:
						if(table[1][0]==' ' && select==4){
							if(number==1){
								table[1][0]='x';
								number--;
							}
							else if(number==0){
								table[1][0]='o';
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[1][0]!=' ' && select==4){
							System.out.println("Silahkan mengisi satu ruang yang kosong ");
						}break;
					case 5:
						if(table[1][1]==' ' && select==5){
							if(number==1){
								table[1][1]='x';
								number--;
							}
							else if(number==0){
								table[1][1]='o';
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[1][1]!=' ' && select==5){
							System.out.println("Silahkan mengisi satu ruang yang kosong ");
						}break;
					case 6:
						if(table[1][2]==' ' && select==6){
							if(number==1){
								table[1][2]='x';
								number--;
							}
							else if(number==0){
								table[1][2]='o';
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[1][2]!=' ' && select==6){
							System.out.println("Silahkan mengisi satu ruang yang kosong ");
						}break;
					case 7:
						if(table[2][0]==' ' && select==7){
							if(number==1){
								table[2][0]='x';
								number--;
							}
							else if(number==0){
								table[2][0]='o';
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[2][0]!=' ' && select==7){
							System.out.println("Silahkan mengisi satu ruang yang kosong ");
						}break;
					case 8:
						if(table[2][1]==' ' && select==8){
							if(number==1){
								table[2][1]='x';
								number--;
							}
							else if(number==0){
								table[2][1]='o';
								number++;
							}
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[2][1]!=' ' && select==8){
							System.out.println("Silahkan mengisi satu ruang yang kosong ");
						}break;
					case 9:
						if(table[2][2]==' ' && select==9){
							if(number==1){
								table[2][2]='x';
								number--;
							}
							else if(number==0){
								table[2][2]='o';
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(table[2][2]!=' ' && select==9){
							System.out.println("Silahkan mengisi satu ruang yang kosong :) ");
						}break;
					default:
						System.out.println("Silahkan pilih nomor yang benar");
						break;
					}				
				}
				
				if(choice2.contentEquals ("ya") ){
					continue;
				}
				else if(choice2.contentEquals ("tidak") ){
					break;
				}
				
			}while(true);
			
		}catch(NoSuchElementException e){
			
		}
	}
	
	public static void main(String[] args) {
			try{
				do{
					int select ;
					
					System.out.println("\n");
					System.out.println("===========================================");
					System.out.println("-------------TIC_TAC_TOE_GAME--------------");
					System.out.println("===========================================");
					System.out.println("1. Mulai Bermain ");
					System.out.println("0. Keluar ");
					System.out.print("Silahkan pilih nomor : ");
					try{
						select=input.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Silahkan masukkan salah satu nomor '1' atau '0'");
						input.nextLine();
						continue;
					}catch(NoSuchElementException e){
						System.out.println();
						System.out.println("You pressed Ctrl+Z");
						System.out.println("Program Terminated");
						break;
					}
					if (select == 0){
						break;
					}
					switch(select){
					case 1:
						input.nextLine();
						playgame();
						break;
					case 0:
						break;
					default:
					 	System.out.println("\n");
						System.out.println("Silahkan masukkan salah satu nomor  '1' atau '0");
						break;
					}
				
				}while(true);
				
				
			
		}catch(NoSuchElementException e){
			System.out.println();
			System.out.println("You pressed Ctrl+Z");
			System.out.println("Program Terminated");
		}
    }	
}
		