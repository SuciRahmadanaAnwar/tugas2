import java.util.*;
public class Matrix {
	static Scanner input=new Scanner(System.in);
	static void MainMenu(){
	while(true){
		int select;
		System.out.println("\t\t   =====MATRIKS=====");
		System.out.println("===========================================");			
		System.out.println("1. Penjumlahan Matriks");
		System.out.println("2. Penguranga Matriks");
		System.out.println("3. Perkalian Matriks");
		System.out.println("0. Keluar");
		System.out.println("===========================================");	
		System.out.print("Silahkan masukkan nomor yang anda pilih : ");
		do{
			System.out.print(">>>>> ");
			try{
				select=input.nextInt();
				if(select!= 1 && select!= 2 && select!= 3  && select!= 0 ){
					System.out.println("Anda Hanya bisa Memilih Nomor '1', '2', '3', atau '0'");
					continue;
				}
				break;
			}catch(InputMismatchException e){
				System.out.println("Anda Hanya bisa Memilih Nomor '1', '2', '3', atau '0' ");
				input.nextLine();
				continue;
			}
		}while(true);
		
		if(select== 0){
			break;
		}
		
		switch(select){
			case 1:
				addition();
				break;
			case 2:
				subtraction();
				break;
			case 3:
				multiplication();
				break;	
		}	
	}
}

static void addition(){
	int rows,columns;
	do{
		System.out.println("\t\t  Penjumlahan Matriks");
		System.out.println("===========================================");

		do{	
			try{
				
				System.out.println("Masukkan Nilai Baris Matriks  ");
				System.out.print(">>>>> ");
				rows=input.nextInt();
					if (rows <1){
						throw new InputMismatchException();
					}
				System.out.println("Masukkan Nilai Kolom Matriks ");
				System.out.print(">>>>> ");
				columns=input.nextInt();
					if (columns <1){
						throw new InputMismatchException();
					}
				break;
			}catch(InputMismatchException e){
				System.out.println("Maaf Inputan Anda Salah :) !!! Silahkan Input Bilangan Asli ( > 0 )");
				input.nextLine();
				continue;
			}
		}while(true);
		
		double[][]matrix1= new double[rows][columns];
		double[][]matrix2= new double[rows][columns];
		double[][]matrixResult=new double[rows][columns];
		System.out.println("\t\t MATRIKS 1");
		do{
			try{
				for(int rowindex=0; rowindex<rows;rowindex++){
					for(int y=0; y<columns;y++){
						System.out.println("Input Nilai Baris ke-"+(rowindex+1)+" dan kolom ke-"+(y+1));
						System.out.print(">> ");
						matrix1[rowindex][y]=input.nextDouble();
					}
				}
				break;
			}catch(InputMismatchException e){
				System.out.println("Silahkan Masukkan Bilangan Riil");
				input.nextLine();
				continue;
			}	
		}while(true);
		
		System.out.println();
		for(int rowindex=0; rowindex<rows;rowindex++){
			for(int y=0; y<columns;y++){
				
				System.out.printf("%.2f  ",matrix1[rowindex][y]);
			}System.out.println();
		}
		
		
		System.out.println();
		
		System.out.println("\t\t MATRIKS 2");
		do{
			try{
				for(int rowindex=0; rowindex<rows;rowindex++){
					for(int y=0; y<columns;y++){
						System.out.println("Masukkan Nilai Baris ke-"+(rowindex+1)+" dan kolom ke-"+(y+1));
						System.out.print(">> ");
						matrix2[rowindex][y]=input.nextDouble();
						matrixResult[rowindex][y]=matrix1[rowindex][y] + matrix2[rowindex][y];
					}
				}
				break;
			}catch(InputMismatchException e){
				System.out.println("Silahkan Masukkan Bilangan Riil");
				input.nextLine();
				continue;
			}	
		}while(true);
		
		System.out.println();
		for(int rowindex=0; rowindex<rows;rowindex++){
			for(int y=0; y<columns;y++){
				
				System.out.printf("%.2f  ",matrix2[rowindex][y]);
			}System.out.println();
		}
		
		System.out.println("\n");
		System.out.println("Hasil dari Penjumlahan Dua Matriks di atas adalah ");
		System.out.println();
		for(int rowindex=0; rowindex<rows;rowindex++){
			for(int y=0; y<columns;y++){
				
				System.out.printf("%.2f  ",matrixResult[rowindex][y]);
			}System.out.println();
		}
		System.out.println();
		String choice;
		input.nextLine();
		do{
			System.out.println("Coba Lagi ? (ya/tidak)");
			choice=input.nextLine();
			if (choice.contentEquals("ya")){
				 break;
			 }
			 else  if (choice.contentEquals("tidak")){
				 break;
			 }
			 else{
				 System.out.println("Maaf, Anda Hanya Bisa Input Kata 'ya' atau 'tidak'");
				 continue;
			 }
		}while(true);
		 if (choice.contentEquals("tidak")){
			 break;
		 }
		 else  if (choice.contentEquals("ya")){
			 continue;
		 }
	}while(true);		
}

static void subtraction(){
	int rows,columns;
	do{
		System.out.println("\t\t  Pengurangan Matriks");
		System.out.println("===========================================");
		do{	
			try{
				
				System.out.println("Input Nilai Baris Matriks ");
				System.out.print(">> ");
				rows=input.nextInt();
					if (rows <1){
						throw new InputMismatchException();
					}
				System.out.println("Input Nilai Kolom Matriks ");
				System.out.print(">> ");
				columns=input.nextInt();
					if (columns <1){
						throw new InputMismatchException();
					}
				break;
			}catch(InputMismatchException e){
				System.out.println("Maaf Inputan Anda Salah!!! Silahkan Input Bilangan Asli ( > 0 )");
				input.nextLine();
				continue;
			}
		}while(true);
		
		double[][]matrix1= new double[rows][columns];
		double[][]matrix2= new double[rows][columns];
		double[][]matrixResult=new double[rows][columns];
		System.out.println("\t\t MATRIKS 1");
		do{
			try{
				for(int rowindex=0; rowindex<rows;rowindex++){
					for(int y=0; y<columns;y++){
						System.out.println("Input Nilai Baris Ke-"+(rowindex+1)+" dan Kolom ke-"+(y+1));
						System.out.print(">> ");
						matrix1[rowindex][y]=input.nextDouble();
					}
				}
				break;
			}catch(InputMismatchException e){
				System.out.println("Silahkan Input Bilangan Riil");
				input.nextLine();
				continue;
			}	
		}while(true);
		
		System.out.println();
		for(int rowindex=0; rowindex<rows;rowindex++){
			for(int y=0; y<columns;y++){		
				System.out.printf("%.2f  ",matrix1[rowindex][y]);
			}System.out.println();
		}
		System.out.println();
		System.out.println("\t\t MATRIKS 2");
		do{
			try{
				for(int rowindex=0; rowindex<rows;rowindex++){
					for(int y=0; y<columns;y++){
						System.out.println("Input Nilai Baris ke-"+(rowindex+1)+" dan Kolom ke-"+(y+1));
						System.out.print(">> ");
						matrix2[rowindex][y]=input.nextDouble();
						matrixResult[rowindex][y]= matrix1[rowindex][y] - matrix2[rowindex][y];
					}
				}
				break;
			}catch(InputMismatchException e){
				System.out.println("Silahkan Input Bilangan Riil");
				input.nextLine();
				continue;
			}	
		}while(true);
		
		System.out.println();
		for(int rowindex=0; rowindex<rows;rowindex++){
			for(int y=0; y<columns;y++){
				System.out.printf("%.2f  ",matrix2[rowindex][y]);
			}System.out.println();
		}
		
		System.out.println("\n");
		System.out.println("Hasil dari Pengurangan Dua Matriks di atas adalah ");
		System.out.println();		
		
		for(int rowindex=0; rowindex<rows;rowindex++){
			for(int y=0; y<columns;y++){
				
				System.out.printf("%.2f  ",matrixResult[rowindex][y]);
			}System.out.println();
		}				
		System.out.println();
		String choice;
		input.nextLine();
		do{
			System.out.println("Coba Lagi ? (ya/tidak)");
			choice=input.nextLine();
			if (choice.contentEquals("tidak")){
				 break;
			 }
			 else  if (choice.contentEquals("ya")){
				 break;
			 }
			 else{
				 System.out.println("Maaf, Anda Hanya Bisa Input Kata 'ya' atau 'tidak'");
				 continue;
			 }
		}while(true);

			 if (choice.contentEquals("tidak")){
				 break;
			 }
			 else  if (choice.contentEquals("ya")){
				 continue;
			 }
	}while(true);
}
static void multiplication(){
	
	while(true){
		 int w=0,choice=0,rows1,columns1,rows2,columns2;
	 do{
		 do{
			 try{
				 System.out.print("Baris Matriks 1 : "); //  rows1*columns1   rows1   rows2*columns2
				 rows1=input.nextInt();
				 System.out.print("Kolom Matriks 1 : ");
				 columns1=input.nextInt();
				 System.out.print("Baris Matriks 2 : ");
				 rows2=input.nextInt();
				 System.out.print("Kolom Matriks 2 : ");
				 columns2=input.nextInt();
				 break;
			 }catch(InputMismatchException e){
				 System.out.println("Silahkan Masukkan Bilangan Bulat");
				 input.nextLine();
				 continue;
			 }
		 }while(true);
		
				if (columns1 != rows2) {
					System.out.println("Jumlah Baris matriks 1 Tidak Sama Dengan Jumlah Kolom matriks 2");
					System.out.println("Matriks Tidak Dapat Di kalikan");					
					do{
						try{
							System.out.print("tekan '0' untuk mengulang Proses atau '1' untuk berhenti : ");
							choice=input.nextInt();
							if (choice==1)break;
							if (choice==0)break;
						}catch(InputMismatchException e){
							System.out.println("Input Satu Angka Yang Benar");
							input.nextLine();
							continue;
						}
					}while((choice!=0)||(choice!=1));
				}			
				if (choice==1)break;
				if (choice==0)continue;
	 }while(columns1!=rows2);
		 if (choice==1)break;
		 System.out.println("\n");
		 
	int[][]matrix=new int[rows1][columns1];
	do{
		System.out.println("Matriks 1: ");
		for(int rowindex=0;rowindex<rows1;rowindex++){
			for(int columnindex=0;columnindex<columns1;columnindex++){
				System.out.print("Baris "+(rowindex+1)+" Kolom "+(columnindex+1)+" : ");
				try{
					matrix[rowindex][columnindex]=input.nextInt();
				}catch(InputMismatchException e){
					System.out.println("Maaf Anda Hanya Dapat Input Bilangan Bulat");
					input.nextLine();
					columnindex--;
					continue;
				}
			}
		}
		break;
	}while(true);
	
	
	System.out.println();
	
	for(int rowindex=0;rowindex<rows1;rowindex++){
		for(int y=0;y<columns1;y++){
			System.out.printf("%4d ", matrix[rowindex][y]);
		}System.out.println();
	}System.out.println();
	
	int[][]matrix2=new int[rows2][columns2];
	do{
		System.out.println("Matriks 2: ");
		for(int rowindex=0;rowindex<rows2;rowindex++){
			for(int columnindex=0;columnindex<columns2;columnindex++){
				System.out.print("baris "+(rowindex+1)+" kolom "+(columnindex+1)+" : ");
				try{
					matrix2[rowindex][columnindex]=input.nextInt();
				}catch(InputMismatchException e){
					System.out.println("Maaf Anda Hanya Dapat Input Bilangan Bulat");
					input.nextLine();
					columnindex--;
					continue;
				}
				
			}
		}
		break;
	}while(true);

	System.out.println();
		
	for(int rowindex=0;rowindex<rows2;rowindex++){
		for(int y=0;y<columns2;y++){
			System.out.printf("%4d ", matrix2[rowindex][y]);
		}System.out.println();
	}
			
	int[][]matrix3=new int[rows1][columns2];
	for(int rowindex=0;rowindex<rows1;rowindex++){
		for(int y=0;y<columns2;y++){
			w=0;
			for(int multiplicationindex=0;multiplicationindex<columns1;multiplicationindex++){
				w+=matrix[rowindex][multiplicationindex]*matrix2[multiplicationindex][y];
			}
			matrix3[rowindex][y]=w;
				}
			}System.out.println();
			
			
		
	System.out.println("Perkalian Matriks ("+rows1+"x"+columns1+") X ("+rows2+"x"+columns2+") menghasilkan Matriks "+rows1+"x"+columns2+" >> ");
	System.out.println("Hasil dari Perkalian Dua Matriks di atas adalah :\n");
	for(int rowindex=0;rowindex<rows1;rowindex++){
		for(int y=0;y<columns2;y++){
			System.out.printf("%4d ", matrix3[rowindex][y]);
		}System.out.println();
		}System.out.println("\n");				
		
		do{
			try{
				System.out.print("tekan '0' untuk mengulang Proses atau '1' untuk berhenti : ");
				choice=input.nextInt();
				if (choice==0)break;
				if (choice==1)break;
			}catch(InputMismatchException e){
				System.out.println("Input Satu Angka Yang Benar");
				input.nextLine();
				continue;
			}
			
		}while((choice<0)||(choice>1));
		
		if (choice==0)break;
		if(choice==1)continue;
		System.out.println();
	 }
}


public static void main(String[] args){
	MainMenu();
}	
}
